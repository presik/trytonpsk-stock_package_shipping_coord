#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright 2018 Oscar Alvarez <oscar@oscar-pc>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import hashlib
from decimal import Decimal
from zeep import Client

wsdl = 'http://sandbox.coordinadora.com/agw/ws/guias/1.6/server.php?wsdl'

id_remitente = 0
ciudad_remitente = '05001000'
ciudad_destinatario = '05001000'


class Notification(object):

    def __init__(self):
        self.client = Client(wsdl=wsdl)
        self.arrayTypeGuiaDetalle = self.client.get_type('ns0:ArrayOfAgw_typeGuiaDetalle')
        self.typeGuiaDetalle = self.client.get_type('ns0:Agw_typeGuiaDetalle')

    def send(self, shipment, client_id, user, sha_signature):
        valor_declarado = 0
        detalle = self.typeGuiaDetalle(
            ubl=0,
            alto=10,
            largo=10,
            ancho=10,
            peso=3,
            unidades=1,
            referencia='UNIFORMES - UTILES',
            nombre_empaque='Caja'
        )
        for move in shipment.outgoing_moves:
            valor_declarado += (move.product.list_price * Decimal(move.quantity))

        detalles = self.arrayTypeGuiaDetalle([detalle])
        company = shipment.company.party

        content = 'Paquete de uniformes'

        _error = None
        customer = shipment.customer
        if not company.addresses[0].street:
            _error = 'Falta la direccion del remitente'
        if not company.phone:
            _error = 'Falta el telefono del remitente'
        if not shipment.warehouse.address.street:
            _error = 'Falta la direccion de la bodega'
        if not customer.department_code:
            _error = 'Falta el departamento del destinatario'
        if not customer.city_name:
            _error = 'Falta la ciudad del destinatario'
        if not customer.addresses[0].street:
            _error = 'Falta la direccion del destinatario'
        if not shipment.planned_date:
            _error = 'Falta la fecha planeada'

        if _error:
            return _error

        res = self.client.service.Guias_generarGuia({
            'usuario': user,
            'clave': sha_signature,
            'id_cliente': client_id,
            'id_remitente': 0,
            'nit_remitente': company.id_number,
            'codigo_remision': '',
            'nombre_remitente': company.name,
            'direccion_remitente': shipment.warehouse.address.street,
            'ciudad_remitente': ciudad_remitente, #shipment.warehouse.address.city_code.name,
            'telefono_remitente': company.phone or '',
            'fecha': shipment.planned_date,
            'nit_destinatario': customer.id_number,
            'div_destinatario': customer.department_code,
            'nombre_destinatario': customer.name,
            'direccion_destinatario': customer.addresses[0].street,
            'ciudad_destinatario': ciudad_destinatario, #shipment.customer.city_name,
            'telefono_destinatario': customer.phone,
            'valor_declarado': str(valor_declarado),
            'codigo_producto': 0,
            'contenido': content,
            'referencia': shipment.reference or '',
            'codigo_cuenta': 1,
            'cuenta_contable': '',
            'centro_costos': '',
            'recaudos': [],
            'nivel_servicio': 1,
            'observaciones': '',
            'estado': 'IMPRESO',
            'detalle': detalles,
            'linea': '',
            'margen_izquierdo': 0,
            'margen_superior': 0,
            'usuario_vmi': '',
            'formato_impresion': '',
            'atributo1_nombre': '',
            'atributo1_valor': '',
            'notificaciones': [],
            'atributos_retorno': [],
            'nro_doc_radicados': '',
            'nro_sobre': ''
        })
        return res
