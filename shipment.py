# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import hashlib

from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, fields
from trytond.pyson import Eval
from .carrier_ws import Notification
from .exceptions import NotificationError
from trytond.i18n import gettext


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'
    id_remision = fields.Char('ID Remision ', states={
        'readonly': True
    })
    codigo_remision = fields.Char('Codigo de Remision ', states={
        'readonly': True
    })
    doc_remision = fields.Char('Doc Remision ', states={
        'readonly': True
    })

    @classmethod
    def __setup__(cls):
        super(ShipmentOut, cls).__setup__()
        cls._buttons.update({
            'send_notification': {
                'invisible': Eval('state') != 'draft',
                },
            })

    @classmethod
    @ModelView.button
    def send_notification(cls, shipments):
        config = Pool().get('stock.configuration')(1)
        notification = Notification()
        if not config.password_carrier_ws or not config.user_carrier_ws or not config.client_id_carrier_ws:
            raise NotificationError(
                gettext('stock_package_shipping_coord.msg_fail_send_notification', s='Missing user or password'))

        sha_signature = hashlib.sha256(config.password_carrier_ws.encode()).hexdigest()
        for shipment in shipments:
            res = notification.send(shipment, config.client_id_carrier_ws,
                config.user_carrier_ws, sha_signature)
            if hasattr(res, 'id_remision'):
                cls.write([shipment], {
                    'id_remision': str(res.id_remision),
                    'codigo_remision': res.codigo_remision,
                    'doc_remision': res.url_terceros
                })
            else:
                raise NotificationError(
                    gettext('stock_package_shipping_coord.msg_fail_send_notification', s=res))
